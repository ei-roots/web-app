exports.getCountryList = () => {
  return fetch('https://restcountries.eu/rest/v2/all', {
    method: "GET"
  })
    .then(r => r.json())
    .then(list => list.map(c => [c.name, c.callingCodes[0]]))
    .catch(err => console.error(err))
}