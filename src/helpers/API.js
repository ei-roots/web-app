import axios from "axios";

const URI = "http://localhost:8000"

export const login = (username, password) => {
  return axios.post(`${URI}/auth/signin`, {
    username,
    password
  })
    .then(r => r.data)
    .catch(err => console.error(err))
}

export const signup = (values) =>{
  return axios.post(`${URI}/auth/signup`,{
    ...values,
    "profilePic": null,
    "verificationCode": null,
    "status": 0,
    "userType": 1,
    "companyID": null,
    "birthDate": "2000-12-13",
    "preferredLanguage": "En"
  })
  .then(r => r.data)
    .catch(err => console.error(err))
}

export const signIn = (username, password) => {
  return axios.get(`${URI}/auth/signin`, {
    method: "POST",
    body: {
      username,
      password
    }
  })
    .then(r => r.json())
    .catch(err => console.error(err))
}

export const signOut = () => {
  return axios.get(`${URI}/auth/signout`, {
    method: "GET"
  })
    .then(r => r.json())
    .catch(err => console.error(err))
}

export const getDerailerQuestions = () => {
  return axios.get(`${URI}/derailer-question/`, {
    headers: {
      "Authorization": `Bearer ${localStorage.getItem("token")}`
    }
  })
    .then(r => r.data)
    .catch(err => console.error(err))
}

export const getEmotionalStylesQuestions = () => {
  return axios.get(`${URI}/emotional-styles-question/`, {
    headers: {
      "Authorization": `Bearer ${localStorage.getItem("token")}`
    }
  })
    .then(r => r.data)
    .catch(err => console.error(err))
}

export const getCompetencyQuestions = () => {
  return axios.get(`${URI}/competency-question/`, {
    headers: {
      "Authorization": `Bearer ${localStorage.getItem("token")}`
    }
  })
    .then(r => r.data)
    .catch(err => console.error(err))
}

export const getSelfAsessmentQuestions = () => {
  return axios.get(`${URI}/self-assessment/`, {
    headers: {
      "Authorization": `Bearer ${localStorage.getItem("token")}`
    }
  })
    .then(r => r.data)
    .catch(err => console.error(err))
}