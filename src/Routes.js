import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

// containers
import Home from './containers/Home'
import AboutUs from './containers/AboutUs'
import Blog from './containers/Blog'
import ContactUs from './containers/ContactUs'
import FAQ from './containers/FAQ'
import Services from './containers/Services'
import Login from './containers/Login'
import Derailer from './containers/Derailer'
import SelfAssessment from './containers/SelfAssessment'
import EmotionalStyles from './containers/EmotionalStyles'
import Competency from './containers/Competency'
import Registration from './containers/Registration'
export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/about" exact component={AboutUs} />
        <Route path="/blog" exact component={Blog} />
        <Route path="/contact" exact component={ContactUs} />
        <Route path="/faq" exact component={FAQ} />
        <Route path="/login" exact component={Login} />
        <Route path="/registration" exact component={Registration} />
        <Route path="/services" exact component={Services} />
        <Route path="/derailer" exact component={Derailer} />
        <Route path="/selfAssessment" exact component={SelfAssessment} />
        <Route path="/emotionalStyles" exact component={EmotionalStyles} />
        <Route path="/competency" exact component={Competency} />
      </Switch>
    </BrowserRouter>
  )
}
