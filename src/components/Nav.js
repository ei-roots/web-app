import React from 'react'
import { Link } from 'react-router-dom'

export default function Nav() {
  return (
    <header className="site-header mo-left header-transparent">
      <div className="container">
        <div className="top-bar">
          <div className="row d-flex justify-content-between align-items-center">
            <div className="dlab-topbar-left">
              <ul>
                <li><i className="la la-envelope"></i> info@example.com</li>
                <li><i className="la la-phone-volume"></i> +91 987-654-3210</li>
              </ul>
            </div>
            <div className="dlab-topbar-right">
              <ul className="dlab-social-icon">
                <li><Link to="/" className="fa fa-facebook"></Link></li>
                <li><Link to="/" className="fa fa-instagram"></Link></li>
                <li><Link to="/" className="fa fa-twitter"></Link></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="sticky-header main-bar-wraper navbar-expand-lg">
        <div className="main-bar clearfix ">
          <div className="container clearfix">
            <div className="logo-header mostion">
              <Link to="/"><img className="logo-2" src={process.env.PUBLIC_URL + "/images/logo-white.png"} alt="Logo" /></Link>
            </div>
            <button className="navbar-toggler collapsed navicon justify-content-end" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
              <span></span>
              <span></span>
              <span></span>
            </button>
            <div className="extra-nav">
              <div className="extra-cell">
                {
                  localStorage.getItem("user") ? <span>{JSON.parse(localStorage.getItem("user")).firstName} {JSON.parse(localStorage.getItem("user")).lastName}</span> :
                    <Link to="/login" href="contact-us.html" className="btn btn-light rounded-xl shadow">LOGIN</Link>
                }
              </div>
            </div>
            <div className="header-nav navbar-collapse collapse justify-content-end" id="navbarNavDropdown">
              <div className="logo-header">
                <Link to="/"><img src={process.env.PUBLIC_URL + "/images/logo-2.png"} alt="Logo" /></Link>
              </div>
              <ul className="nav navbar-nav navbar">
                <li className="active">
                  <Link to="/"><span>Home</span></Link>
                </li>
                <li className="active">
                  <Link to="/about"><span>About Us</span></Link>
                </li>
                <li>
                  <Link to="/services"><span>Services</span></Link>
                </li>
                <li>
                  <Link to="/faq"><span>FAQ</span></Link>
                </li>

                <li>
                  <Link to="/blog"><span>Blog</span></Link>
                </li>
                <li>
                  <Link to="/contact"><span>Contact Us</span></Link>
                </li>
              </ul>
              <div className="dlab-social-icon">
                <ul>
                  <li><Link to="/" className="fa fa-facebook"></Link></li>
                  <li><Link to="/" className="fa fa-twitter"></Link></li>
                  <li><Link to="/" className="fa fa-linkedin"></Link></li>
                  <li><Link to="/" className="fa fa-instagram"></Link></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}
