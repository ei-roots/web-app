import React from 'react'
import { Link } from 'react-router-dom'

export default function Footer() {
  return (
    <footer className="site-footer style-2" id="footer" style={{ backgroundImage: `url(${process.env.PUBLIC_URL}/images/background/bg4.png)` }}>
      <div className="container">
        <div className="dlab-subscribe style-1 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
          <div className="row align-items-center">
            <div className="col-lg-7">
              <h2 className="title">Subscribe To Our Newsletter For Latest Update</h2>
            </div>
            <div className="col-lg-5">
              <form className="dzSubscribe" action="scripts/mailchamp.php" method="post">
                <div className="dzSubscribeMsg"></div>
                <div className="form-group">
                  <div className="input-group">
                    <input name="dzEmail" required="required" type="email" className="form-control" placeholder="Your Email Address" />
                    <div className="input-group-addon">
                      <button name="submit" value="Submit" type="submit" className="btn btn-primary gradient fa fa-paper-plane-o"></button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-top">
        <div className="container">
          <div className="row">
            <div className="col-xl-3 col-lg-4 col-sm-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
              <div className="widget widget_about">
                <div className="footer-logo">
                  <Link to="/"><img src={process.env.PUBLIC_URL + "images/logo-white.png"} alt="" /></Link>
                </div>
                <div className="widget widget_getintuch">
                  <ul>
                    <li>
                      <i className="fa fa-phone gradient"></i>
                      <span>+91 123-456-7890<br />+91 987-654-3210</span>
                    </li>
                    <li>
                      <i className="fa fa-envelope gradient"></i>
                      <span>info@example.com <br />info@example.com</span>
                    </li>
                    <li>
                      <i className="fa fa-map-marker gradient"></i>
                      <span>Marmora Road Chi Minh City, Vietnam</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-lg-2 col-sm-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
              <div className="widget widget_services">
                <h5 className="footer-title">Our links</h5>
                <ul>
                  <li><Link to="/">Home</Link></li>
                  <li><Link to="/">About Us</Link></li>
                  <li><Link to="/">Services</Link></li>
                  <li><Link to="/">Team</Link></li>
                  <li><Link to="/">Blog</Link></li>
                </ul>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-sm-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.6s">
              <div className="widget widget_services">
                <h5 className="footer-title">Our Services</h5>
                <ul>
                  <li><Link to="/">Strategy & Research</Link></li>
                  <li><Link to="/">Web Development</Link></li>
                  <li><Link to="/">Web Solution</Link></li>
                  <li><Link to="/">Digital Marketing</Link></li>
                  <li><Link to="/">App Design </Link></li>
                </ul>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-sm-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.8s">
              <div className="widget widget_services">
                <h5 className="footer-title">Other links</h5>
                <ul>
                  <li><Link to="/">FAQ</Link></li>
                  <li><Link to="/">Portfolio</Link></li>
                  <li><Link to="/">Privacy Policy</Link></li>
                  <li><Link to="/">Terms & Conditions</Link></li>
                  <li><Link to="/">Support </Link></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-7 text-left">
              <span className="copyright-text">Copyright © 2021 <Link to="/" target="_blank">EI ROOTS</Link>. All rights reserved.</span>
            </div>
            <div className="col-lg-6 col-md-5 text-right">
              <div className="dlab-social-icon">
                <ul>
                  <li><Link to="/" className="fa fa-facebook"></Link></li>
                  <li><Link to="/" className="fa fa-instagram"></Link></li>
                  <li><Link to="/" className="fa fa-twitter"></Link></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}
