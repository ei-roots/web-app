import { nanoid } from 'nanoid'
import React from 'react'

export default function QuestionCard(props) {
  const { type, question } = props
  const key = nanoid()

  if (type === "derailer")
    return (
      <tr>
        <th className="breadcrumb-item" scope="row">{question}</th> 
        <th>
            <div className="form-check">
              <label className="radiocontainer">
                <input type="radio" name={key} className="form-check-input" />
                  <span className="checkmark"></span>
              </label>
            </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
      </tr> 
    )
  else if (type === "emotionalStyles")
    return (
      <tr>
        <th className="breadcrumb-item" scope="row">{question}</th> 
        <th>
            <div className="form-check">
              <label className="radiocontainer">
                <input type="radio" name={key} className="form-check-input" />
                  <span className="checkmark"></span>
              </label>
            </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
      </tr>      
    )
  else if (type === "selfAssessment")
    return (
      <tr>
        <th className="breadcrumb-item" scope="row">{question}</th> 
        <th>
            <div className="form-check">
              <label className="radiocontainer">
                <input type="radio" name={key} className="form-check-input" />
                  <span className="checkmark"></span>
              </label>
            </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
      </tr>  
    )
  else if (type === "competency")
    return (
      <tr>
        <th className="breadcrumb-item" scope="row">{question}</th> 
        <th>
            <div className="form-check">
              <label className="radiocontainer">
                <input type="radio" name={key} className="form-check-input" />
                  <span className="checkmark"></span>
              </label>
            </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
        <th>
          <div className="form-check">
            <label className="radiocontainer">
              <input type="radio" name={key} className="form-check-input" />
                <span className="checkmark"></span>
            </label>
          </div>
        </th>
      </tr> 
    )
  else
    return (
      <div>Not available</div>
    )
}
