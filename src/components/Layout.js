import React from 'react'
import Footer from './Footer'
import Nav from './Nav'

export default function Layout({ children }) {
  return (
    <div id="bg">
      <div id="loading-area" className="loading-02"></div>
      <div className="page-wrapper">
        <Nav />
        <div className="page-content bg-white">
          {children}
        </div>
        <Footer />
        <button className="scroltop icon-up" type="button"><i className="fa fa-arrow-up"></i></button>
      </div>
    </div>
  )
}
