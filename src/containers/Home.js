import React from 'react'
import Layout from '../components/Layout'
import { Link } from 'react-router-dom'

export default function Home() {
	return (
		<Layout>
			<div className="banner-two gradient">
				<div className="container">
					<div className="banner-inner">
						<img className="img1 move-2" src={process.env.PUBLIC_URL + "images/main-slider/slider2/pic3.png"} alt="" />
						<div className="row align-items-center">
							<div className="col-lg-6">
								<div className="banner-content text-white">
									<h6 className="wow fadeInUp sub-title bgl-light text-white" data-wow-delay="0.5s">We are The Best</h6>
									<h1 className="wow fadeInUp m-b20" data-wow-delay="1s">We Provide Website Solution For You</h1>
									<p className="wow fadeInUp m-b30" data-wow-delay="1.5s">Mauris in enim sollicitudin quam sollicitudin fermentum ut vitae massa. Donec venenatis accumsan nisi, sit amet maximus velit euismod sit amet. Vivamus et sem sed ipsum pretium lobortis non vitae sem.</p>
									<Link to="/" className="wow fadeInUp btn btn-light text-primary shadow rounded-xl" data-wow-delay="2s">Learn More</Link>
								</div>
							</div>
						</div>
						<img className="img2 move-1" src={process.env.PUBLIC_URL + "images/pattern/pattern8.png"} alt="" />
						<img className="img3 move-2" src={process.env.PUBLIC_URL + "images/pattern/pattern9.png"} alt="" />
					</div>
				</div>
				<div className="dz-media">
					<img src={process.env.PUBLIC_URL + "images/main-slider/slider2/pic1.jpg"} alt="" />
				</div>
			</div>

			<section className="content-inner-2" style={{ backgroundImage: "url(images/background/bg2.png)" }}>
				<div className="container">
					<div className="row icon-wraper-1">
						<div className="col-lg-4 col-md-6">
							<div className="section-head style-3 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
								<h6 className="sub-title text-primary bgl-primary m-b15">OUR SERVICES</h6>
								<h2 className="title m-t10">We Are Providing Digital Services</h2>
							</div>
						</div>
						<div className="col-lg-4 col-md-6">
							<div className="icon-bx-wraper style-3 m-b30 box-hover wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.4s">
								<div className="icon-bx-sm radius bgl-primary">
									<Link to="/" className="icon-cell">
										<i className="flaticon-office"></i>
									</Link>
								</div>
								<div className="wraper-effect"></div>
								<div className="icon-content">
									<h4 className="dlab-title m-b15">Strategy & Research</h4>
									<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
								</div>
							</div>
						</div>
						<div className="col-lg-4 col-md-6">
							<div className="icon-bx-wraper style-3 m-b30 box-hover active wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
								<div className="icon-bx-sm radius bgl-primary">
									<Link to="/" className="icon-cell">
										<i className="flaticon-pie-charts"></i>
									</Link>
								</div>
								<div className="wraper-effect"></div>
								<div className="icon-content">
									<h4 className="dlab-title m-b15">Web Development</h4>
									<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
								</div>
							</div>
						</div>
						<div className="col-lg-4 col-md-6">
							<div className="icon-bx-wraper style-3 m-b30 box-hover wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.8s">
								<div className="icon-bx-sm radius bgl-primary">
									<Link to="/" className="icon-cell">
										<i className="flaticon-dollar"></i>
									</Link>
								</div>
								<div className="wraper-effect"></div>
								<div className="icon-content">
									<h4 className="dlab-title m-b15">Web Solution</h4>
									<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
								</div>
							</div>
						</div>
						<div className="col-lg-4 col-md-6">
							<div className="icon-bx-wraper style-3 m-b30 box-hover wow fadeInUp" data-wow-duration="2s" data-wow-delay="1.0s">
								<div className="icon-bx-sm radius bgl-primary">
									<Link to="/" className="icon-cell">
										<i className="flaticon-line-graph"></i>
									</Link>
								</div>
								<div className="wraper-effect"></div>
								<div className="icon-content">
									<h4 className="dlab-title m-b15">SEO & Marketing</h4>
									<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
								</div>
							</div>
						</div>
						<div className="col-lg-4 col-md-6">
							<div className="icon-bx-wraper style-3 m-b30 box-hover wow fadeInUp" data-wow-duration="2s" data-wow-delay="1.2s">
								<div className="icon-bx-sm radius bgl-primary">
									<Link to="/" className="icon-cell">
										<i className="flaticon-help"></i>
									</Link>
								</div>
								<div className="wraper-effect"></div>
								<div className="icon-content">
									<h4 className="dlab-title m-b15">Growth Tracking</h4>
									<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section className="content-inner about-wraper-1" style={{ backgroundImage: "url(images/background/bg15.png)", backgroundSize: "contain", backgroundPosition: "center right", backgroundRepeat: "no-repeat" }}>
				<div className="container">
					<div className="row align-items-center">
						<div className="col-lg-6 m-b30 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s">
							<div className="dz-media left">
								<img src={process.env.PUBLIC_URL + "images/about/pic1.jpg"} alt="" />
							</div>
						</div>
						<div className="col-lg-6 m-b30 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.4s">
							<div className="section-head style-3">
								<h6 className="sub-title text-primary bgl-primary m-b15">ABOUT US</h6>
								<h2 className="title m-b20">We Have Creative Team To Build Your Website Better</h2>
								<p>Proin laoreet leo vel enim gravida, at porttitor metus ultricies. Cras eu velit enim. Vivamus blandit, dolor ut aliquet rutrum, ex elit mattis sapien, non molestie felis neque et nulla. Sed euismod turpis id nibh suscipit semper. Pellentesque dapibus risus arcu.</p>
							</div>
							<ul className="list-check primary m-b30">
								<li>Suspendisse ullamcorper mollis orci in facilisis.</li>
								<li>Etiam orci magna, accumsan varius enim volutpat.</li>
								<li>Donec fringilla velit risus, in imperdiet turpis euismod quis.</li>
								<li>Aliquam pulvinar diam tempor erat pellentesque, accumsan mauri.</li>
							</ul>
							<Link to="/" className="btn btn-primary rounded-xl gradient">Learn More</Link>
						</div>
					</div>
				</div>
			</section>

			<section className="content-inner-2" style={{ backgroundImage: "url(images/background/bg2.png)", backgroundSize: "contain", backgroundPosition: "center", backgroundRepeat: "no-repeat" }}>
				<div className="container">
					<div className="row align-items-center">
						<div className="col-lg-5 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s">
							<div className="section-head style-3 mb-4">
								<h6 className="sub-title text-primary bgl-primary m-b15">OUR SERVICES</h6>
								<h2 className="title">Behind The Story Of Slack Digital Agency</h2>
							</div>
							<p>Suspendisse potenti. Donec vel massa ut justo efficitur lacinia non ut felis. Etiam euismod, magna et efficitur ullamcorper, justo justo suscipit tellus, quis egestas lectus nulla ac velit. Morbi consequat vehicula tincidunt.</p>
							<p>Suspendisse purus nibh, lacinia ut rutrum eu, tincidunt eleifend ligula. Maecenas tristique est vitae erat semper pellentesque.</p>
							<p className="m-b30">Mauris sollicitudin tincidunt libero ac suscipit. Quisque ullamcorper consequat dui ut molestie.</p>
						</div>
						<div className="col-lg-7">
							<div className="icon-bx-wraper style-3 left box-hover m-r100 m-b30 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.4s">
								<div className="icon-bx-sm radius">
									<Link to="/" className="icon-cell">
										<i className="flaticon-idea"></i>
									</Link>
								</div>
								<div className="wraper-effect"></div>
								<div className="icon-content">
									<h4 className="dlab-title m-b15">Idea &amp; Analysis Gathering</h4>
									<p>Maecenas laoreet efficitur sagittis. Aliquam eleifend nisl leo, sit amet consequat augue mattis varius. Suspendisse rhoncus nisl.</p>
								</div>
							</div>
							<div className="icon-bx-wraper style-3 left box-hover active m-l100 m-b30 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.6s">
								<div className="icon-bx-sm radius">
									<Link to="/" className="icon-cell">
										<i className="flaticon-coding"></i>
									</Link>
								</div>
								<div className="wraper-effect"></div>
								<div className="icon-content">
									<h4 className="dlab-title m-b15">Design &amp; Developing</h4>
									<p>Maecenas laoreet efficitur sagittis. Aliquam eleifend nisl leo, sit amet consequat augue mattis varius. Suspendisse rhoncus nisl.</p>
								</div>
							</div>
							<div className="icon-bx-wraper style-3 left box-hover m-r100 m-b30 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.8s">
								<div className="icon-bx-sm radius">
									<Link to="/" className="icon-cell">
										<i className="flaticon-rocket"></i>
									</Link>
								</div>
								<div className="wraper-effect"></div>
								<div className="icon-content">
									<h4 className="dlab-title m-b15">Testing &amp; Lunching</h4>
									<p>Maecenas laoreet efficitur sagittis. Aliquam eleifend nisl leo, sit amet consequat augue mattis varius. Suspendisse rhoncus nisl.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section className="content-inner-2" style={{ backgroundImage: " url(images/background/bg17.png)", backgroundSize: "contain", backgroundPosition: "center", backgroundRepeat: "no-repeat" }}>
				<div className="container">
					<div className="section-head style-3 text-center">
						<h6 className="sub-title text-primary bgl-primary m-b15">TESTIMONIAL</h6>
						<h2 className="title">See What Our Clients Say’s</h2>
					</div>
					<div className="row">
						<div className="col-md-12">
							<div className="testimonials-wraper-2">
								<div className="swiper-container testimonial-thumbs">
									<div className="swiper-wrapper">
										<div className="swiper-slide">
											<div className="testimonial-pic">
												<img src={process.env.PUBLIC_URL + "images/testimonials/pic1.jpg"} alt="" />
												<div className="shape-bx"></div>
											</div>
										</div>
										<div className="swiper-slide">
											<div className="testimonial-pic">
												<img src={process.env.PUBLIC_URL + "images/testimonials/pic2.jpg"} alt="" />
												<div className="shape-bx"></div>
											</div>
										</div>
										<div className="swiper-slide">
											<div className="testimonial-pic">
												<img src={process.env.PUBLIC_URL + "images/testimonials/pic3.jpg"} alt="" />
												<div className="shape-bx"></div>
											</div>
										</div>
									</div>
								</div>
								<div className="swiper-container testimonial-content">
									<div className="swiper-wrapper">
										<div className="swiper-slide">
											<div className="testimonial-4 quote-right">
												<div className="testimonial-text">
													<strong className="testimonial-name">Cak Dikin</strong>
													<span className="testimonial-position text-primary m-b20">CEO & Founder </span>
													<p>Duis feugiat est tincidunt ligula maximus convallis. Aenean ultricies, mi non vestibulum auctor, erat tortor porttitor ipsum, nec dictum tortor sem eget nunc. Etiam sed facilisis erat. Vestibulum sed posuere augue, ut molestie erat. Nam ipsum tellus, tempus vel ante ut, aliquet finibus dui. Proin lacinia, erat ut feugiat fringilla, tortor eros ultricies sem, sed finibus massa ex sit amet ligula.</p>
												</div>
											</div>
										</div>
										<div className="swiper-slide">
											<div className="testimonial-4 quote-right">
												<div className="testimonial-text">
													<strong className="testimonial-name">Cak Dikin</strong>
													<span className="testimonial-position text-primary m-b20">CEO & Founder </span>
													<p>Duis feugiat est tincidunt ligula maximus convallis. Aenean ultricies, mi non vestibulum auctor, erat tortor porttitor ipsum, nec dictum tortor sem eget nunc. Etiam sed facilisis erat. Vestibulum sed posuere augue, ut molestie erat. Nam ipsum tellus, tempus vel ante ut, aliquet finibus dui. Proin lacinia, erat ut feugiat fringilla, tortor eros ultricies sem, sed finibus massa ex sit amet ligula.</p>
												</div>
											</div>
										</div>
										<div className="swiper-slide">
											<div className="testimonial-4 quote-right">
												<div className="testimonial-text">
													<strong className="testimonial-name">Cak Dikin</strong>
													<span className="testimonial-position text-primary m-b20">CEO & Founder </span>
													<p>Duis feugiat est tincidunt ligula maximus convallis. Aenean ultricies, mi non vestibulum auctor, erat tortor porttitor ipsum, nec dictum tortor sem eget nunc. Etiam sed facilisis erat. Vestibulum sed posuere augue, ut molestie erat. Nam ipsum tellus, tempus vel ante ut, aliquet finibus dui. Proin lacinia, erat ut feugiat fringilla, tortor eros ultricies sem, sed finibus massa ex sit amet ligula.</p>
												</div>
											</div>
										</div>
									</div>
									<div className="swiper-pagination"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section className="content-inner-2 latestnews" style={{ backgroundImage: "url(images/background/bg16.png)", backgroundSize: "cover", backgroundPosition: "top center", backgroundRepeat: "no-repeat" }}>
				<div className="container">
					<div className="section-head style-3 text-center">
						<h6 className="sub-title text-primary bgl-primary m-b15">OUR BLOG</h6>
						<h2 className="title">Latest News & Blog</h2>
					</div>
					<div className="blog-carousel2 owl-carousel owl-none">
						<div className="item">
							<div className="dlab-blog style-2 m-b10">
								<div className="dlab-media rounded-md">
									<Link to="/"><img src={process.env.PUBLIC_URL + "images/blog/blog-grid-2/pic1.jpg"} alt="" /></Link>
								</div>
								<div className="dlab-info">
									<div className="dlab-meta">
										<ul>
											<li className="post-date"><span>25</span> March</li>
											<li className="post-category">
												<Link to="/">Design</Link>
												<Link to="/" >Development</Link>
											</li>
										</ul>
									</div>
									<h4 className="dlab-title"><Link to="/" >Maecenas maximus augue eget libero dictum, vitae tempus erat pretium.</Link></h4>
									<p className="m-b20">Nunc convallis sagittis dui eu dictum. Cras sodales id ipsum ac aliquam. Phasellus justo quam, sagittis vel sem congue, vehicula tempus elit. Pellentesque ut scelerisque ante.</p>
									<Link to="/" className="btn-link style-1">Read More</Link>
								</div>
							</div>
						</div>
						<div className="item">
							<div className="dlab-blog style-2 m-b10">
								<div className="dlab-media rounded-md">
									<Link to="/" ><img src={process.env.PUBLIC_URL + "images/blog/blog-grid-2/pic2.jpg"} alt="" /></Link>
								</div>
								<div className="dlab-info">
									<div className="dlab-meta">
										<ul>
											<li className="post-date"><span>25</span> March</li>
											<li className="post-category">
												<Link to="/" >Design</Link>
												<Link to="/" >Development</Link>
											</li>
										</ul>
									</div>
									<h4 className="dlab-title"><Link to="/">Maecenas maximus augue eget libero dictum, vitae tempus erat pretium.</Link></h4>
									<p className="m-b20">Nunc convallis sagittis dui eu dictum. Cras sodales id ipsum ac aliquam. Phasellus justo quam, sagittis vel sem congue, vehicula tempus elit. Pellentesque ut scelerisque ante.</p>
									<Link to="/" className="btn-link style-1">Read More</Link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</Layout>
	)
}
