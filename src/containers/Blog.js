import React from 'react'
import Layout from '../components/Layout'
import { Link } from 'react-router-dom'

export default function Blog() {
	return (
		<Layout>
			<div className="page-content bg-white">
				<div className="dlab-bnr-inr style-2 overlay-gradient-dark" style={{ backgroundImage: "url(images/banner/bnr1.jpg)" }}>
					<div className="container">
						<div className="dlab-bnr-inr-entry">
							<h1>Blog Grid 2</h1>
							<nav aria-label="breadcrumb" className="breadcrumb-row">
								<ul className="breadcrumb">
									<li className="breadcrumb-item"><Link to="/">Home</Link></li>
									<li className="breadcrumb-item active" aria-current="page">Blog</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
				<section className="content-inner" style={{ backgroundImage: "url(images/background/bg2.png)" }}>
					<div className="container">
						<div className="row">
							<div className="col-lg-6 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
								<div className="dlab-blog style-1 m-b50 bg-white">
									<div className="dlab-info">
										<div className="dlab-meta">
											<ul>
												<li className="post-date"><i className="flaticon-clock m-r10"></i>7 March, 2020</li>
												<li className="post-author"><i className="flaticon-user m-r10"></i>By <Link to="/"> Johne Doe</Link></li>
												<li className="post-comment"><Link to="/"><i className="flaticon-speech-bubble"></i><span>15</span></Link></li>
												<li className="post-share"><i className="flaticon-share"></i>
													<ul>
														<li><Link className="fa fa-facebook" to="/"></Link></li>
														<li><Link className="fa fa-twitter" to="/"></Link></li>
														<li><Link className="fa fa-linkedin" to="/"></Link></li>
													</ul>
												</li>
											</ul>
										</div>
										<h4 className="dlab-title">
											<Link to="/">Donec feugiat mollis nisi in dignissim. Morbi sollicitudin quis lectus vel euismod.</Link>
										</h4>
										<p className="m-b0">Maecenas aliquet quam sed tellus cursus, eget sodales elit luctus. Proin blandit sed arcu sed ultricies. Fusce ac ligula vel enim fermentum blandit. </p>
									</div>
									<div className="dlab-media dlab-img-effect zoom">
										<Link to="/"><img src={process.env.PUBLIC_URL + "images/blog/blog-grid/pic1.jpg"} alt="" /></Link>
									</div>
								</div>
							</div>
							<div className="col-lg-6 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.4s">
								<div className="dlab-blog style-1 m-b50 bg-white">
									<div className="dlab-info">
										<div className="dlab-meta">
											<ul>
												<li className="post-date"><i className="flaticon-clock m-r10"></i>7 March, 2020</li>
												<li className="post-author"><i className="flaticon-user m-r10"></i>By <Link to="/"> Johne Doe</Link></li>
												<li className="post-comment"><Link to="/"><i className="flaticon-speech-bubble"></i><span>15</span></Link></li>
												<li className="post-share"><i className="flaticon-share"></i>
													<ul>
														<li><Link className="fa fa-facebook" to="/"></Link></li>
														<li><Link className="fa fa-twitter" to="/;"></Link></li>
														<li><Link className="fa fa-linkedin" to="/"></Link></li>
													</ul>
												</li>
											</ul>
										</div>
										<h4 className="dlab-title">
											<Link to="/">Quisque egestas iaculis felis eget placerat ut pulvinar mi elitauctor nec.</Link>
										</h4>
										<p className="m-b0">Maecenas aliquet quam sed tellus cursus, eget sodales elit luctus. Proin blandit sed arcu sed ultricies. Fusce ac ligula vel enim fermentum blandit. </p>
									</div>
									<div className="dlab-media dlab-img-effect zoom">
										<Link to="/"><img src="images/blog/blog-grid/pic2.jpg" alt="" /></Link>
									</div>
								</div>
							</div>
							<div className="col-lg-6 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
								<div className="dlab-blog style-1 m-b50 bg-white">
									<div className="dlab-info">
										<div className="dlab-meta">
											<ul>
												<li className="post-date"><i className="flaticon-clock m-r10"></i>7 March, 2020</li>
												<li className="post-author"><i className="flaticon-user m-r10"></i>By <Link to="/"> Johne Doe</Link></li>
												<li className="post-comment"><Link to="/"><i className="flaticon-speech-bubble"></i><span>15</span></Link></li>
												<li className="post-share"><i className="flaticon-share"></i>
													<ul>
														<li><Link className="fa fa-facebook" to="/"></Link></li>
														<li><Link className="fa fa-twitter" to="/"></Link></li>
														<li><Link className="fa fa-linkedin" to="/"></Link></li>
													</ul>
												</li>
											</ul>
										</div>
										<h4 className="dlab-title">
											<Link to="/">Fusce sem ligula, imperdiet sed nisi sit amet, euismod posuere dolor.</Link>
										</h4>
										<p className="m-b0">Maecenas aliquet quam sed tellus cursus, eget sodales elit luctus. Proin blandit sed arcu sed ultricies. Fusce ac ligula vel enim fermentum blandit. </p>
									</div>
									<div className="dlab-media dlab-img-effect zoom">
										<Link to="/"><img src="images/blog/blog-grid/pic3.jpg" alt="" /></Link>
									</div>
								</div>
							</div>
							<div className="col-lg-6 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.8s">
								<div className="dlab-blog style-1 m-b50 bg-white">
									<div className="dlab-info">
										<div className="dlab-meta">
											<ul>
												<li className="post-date"><i className="flaticon-clock m-r10"></i>7 March, 2020</li>
												<li className="post-author"><i className="flaticon-user m-r10"></i>By <Link to="/"> Johne Doe</Link></li>
												<li className="post-comment"><Link to="/"><i className="flaticon-speech-bubble"></i><span>15</span></Link></li>
												<li className="post-share"><i className="flaticon-share"></i>
													<ul>
														<li><Link className="fa fa-facebook" to="/"></Link></li>
														<li><Link className="fa fa-twitter" to="/"></Link></li>
														<li><Link className="fa fa-linkedin" to="/"></Link></li>
													</ul>
												</li>
											</ul>
										</div>
										<h4 className="dlab-title">
											<Link to="/">Praesent ut lobortis purus hasellus libero orci, accumsan.</Link>
										</h4>
										<p className="m-b0">Maecenas aliquet quam sed tellus cursus, eget sodales elit luctus. Proin blandit sed arcu sed ultricies. Fusce ac ligula vel enim fermentum blandit. </p>
									</div>
									<div className="dlab-media dlab-img-effect zoom">
										<Link to="/"><img src="images/blog/blog-grid/pic4.jpg" alt="" /></Link>
									</div>
								</div>
							</div>
							<div className="col-lg-6 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="1.0s">
								<div className="dlab-blog style-1 m-b50 bg-white">
									<div className="dlab-info">
										<div className="dlab-meta">
											<ul>
												<li className="post-date"><i className="flaticon-clock m-r10"></i>7 March, 2020</li>
												<li className="post-author"><i className="flaticon-user m-r10"></i>By <Link to="/"> Johne Doe</Link></li>
												<li className="post-comment"><Link to="/"><i className="flaticon-speech-bubble"></i><span>15</span></Link></li>
												<li className="post-share"><i className="flaticon-share"></i>
													<ul>
														<li><Link className="fa fa-facebook" to="/"></Link></li>
														<li><Link className="fa fa-twitter" to="/"></Link></li>
														<li><Link className="fa fa-linkedin" to="/"></Link></li>
													</ul>
												</li>
											</ul>
										</div>
										<h4 className="dlab-title">
											<Link to="/">Donec feugiat mollis nisi in dignissim. Morbi sollicitudin quis lectus vel euismod.</Link>
										</h4>
										<p className="m-b0">Maecenas aliquet quam sed tellus cursus, eget sodales elit luctus. Proin blandit sed arcu sed ultricies. Fusce ac ligula vel enim fermentum blandit. </p>
									</div>
									<div className="dlab-media dlab-img-effect zoom">
										<Link to="/"><img src="images/blog/blog-grid/pic5.jpg" alt="" /></Link>
									</div>
								</div>
							</div>
							<div className="col-lg-6 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="1.2s">
								<div className="dlab-blog style-1 m-b50 bg-white">
									<div className="dlab-info">
										<div className="dlab-meta">
											<ul>
												<li className="post-date"><i className="flaticon-clock m-r10"></i>7 March, 2020</li>
												<li className="post-author"><i className="flaticon-user m-r10"></i>By <Link to="/"> Johne Doe</Link></li>
												<li className="post-comment"><Link to="/"><i className="flaticon-speech-bubble"></i><span>15</span></Link></li>
												<li className="post-share"><i className="flaticon-share"></i>
													<ul>
														<li><Link className="fa fa-facebook" to="/"></Link></li>
														<li><Link className="fa fa-twitter" to="/"></Link></li>
														<li><Link className="fa fa-linkedin" to="/"></Link></li>
													</ul>
												</li>
											</ul>
										</div>
										<h4 className="dlab-title">
											<Link to="/">Quisque sem tortor, convallis in arcu eu, accumsan finibus massa donec et sapien risus.</Link>
										</h4>
										<p className="m-b0">Maecenas aliquet quam sed tellus cursus, eget sodales elit luctus. Proin blandit sed arcu sed ultricies. Fusce ac ligula vel enim fermentum blandit. </p>
									</div>
									<div className="dlab-media dlab-img-effect zoom">
										<Link to="/"><img src="images/blog/blog-grid/pic6.jpg" alt="" /></Link>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</Layout>
	)
}
