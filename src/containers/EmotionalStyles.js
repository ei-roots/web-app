import { React, useEffect, useState } from 'react'
import Layout from '../components/Layout'
import { Link } from 'react-router-dom'
import QuestionCard from '../components/QuestionCard'
import { getEmotionalStylesQuestions } from '../helpers/API'
import { nanoid } from 'nanoid'

function EmotionalStyles() {
  const [questions, setQuestions] = useState([])

  useEffect(() => {
    getEmotionalStylesQuestions()
      .then(q => setQuestions(q.EIEmotionalStylesQuestions))
      .catch(err => console.error(err))
  }, [])

  return (
    <Layout>
      <div className="page-content bg-white">
        <div className="dlab-bnr-inr style-2 overlay-gradient-dark" style={{ backgroundImage: "url(../images/banner/bnr1.jpg)" }}>
          <div className="container">
            <div className="dlab-bnr-inr-entry">
              <h1>Emotional Styles</h1>
              <nav aria-label="breadcrumb" className="breadcrumb-row">
                <ul className="breadcrumb">
                  <li className="breadcrumb-item"><Link to="/">Tools</Link></li>
                  <li className="breadcrumb-item active" aria-current="page">Emotional Styles</li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <div className="content-inner" style={{ backgroundImage: "url(../images/background/bg2.png)", backgroundRepeat: "no-repeat" }}>
          <div className="container">
            <div className="row align-items-center">
            {
                // <div className="col-lg-6 m-b30 quote-media">
                //   <div className="dlab-media style-1 move-1">
                //     <img src="../images/team/large/pic1.jpg" alt="" />
                //   </div>
                //   <div className="dlab-media style-2 move-2">
                //     <img src="../images/team/large/pic2.jpg" alt="" />
                //   </div>
                //   <div className="dlab-media style-3 move-3">
                //     <img src="../images/team/large/pic3.jpg" alt="" />
                //   </div>
                // </div>
              }
              <div className="col-lg-10 m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                <form className="dlab-form dzForm" method="POST" action="script/contact.php">
                  <div className="dzFormMsg"></div>
                  <div className="row">
                    {
                      questions && (
                        <table className="table table-borderless">
                        <thead>
                          <tr>
                            <th scope="col"></th>
                            <th scope="col">True</th>
                            <th scope="col">False</th>
                          </tr>
                        </thead>
                        {
                          questions.map(question => (
                            <tbody className="col-sm-12 p-2" key={nanoid()}>
                              <QuestionCard type="emotionalStyles" question={question.question} />
                            </tbody>
                          ))
                        }
                      </table>
                      )
                    }
                    
                    <div className="col-sm-12">
                      <button name="submit" value="Submit" className="btn btn-primary gradient border-0 rounded-xl">Submit Now</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default EmotionalStyles
