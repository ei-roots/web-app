import React from 'react'
import Layout from '../components/Layout'
import { Link } from 'react-router-dom'

export default function AboutUs() {
	return (
		<Layout>
			<div className="page-content bg-white">
				<div className="dlab-bnr-inr style-2 overlay-gradient-dark" style={{ backgroundImage: "url(images/banner/bnr1.jpg)" }}>
					<div className="container">
						<div className="dlab-bnr-inr-entry">
							<h1>About Us 2</h1>
							<nav aria-label="breadcrumb" className="breadcrumb-row">
								<ul className="breadcrumb">
									<li className="breadcrumb-item"><Link to="/">Home</Link></li>
									<li className="breadcrumb-item active" aria-current="page">About Us</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
				<section className="content-inner-2">
					<div className="container">
						<div className="section-head style-3 text-center">
							<h6 className="sub-title text-primary bgl-primary m-b15">OUR STEPS</h6>
							<h2 className="title">How It’s Work</h2>
						</div>
						<div className="row about-wraper-3 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
							<div className="col-md-4">
								<div className="icon-bx-wraper style-6 text-center m-b30 icon-up">
									<div className="icon-bx-lg radius bg-white">
										<Link to="/" className="icon-cell text-primary">
											<i className="flaticon-sketch"></i>
										</Link>
									</div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">Designing</h4>
										<p>Suspendisse potenti. Pellentesque ornare mattis elit non fermentum. Mauris rhoncus efficitu.</p>
									</div>
								</div>
							</div>
							<div className="col-md-4">
								<div className="icon-bx-wraper style-6 text-center m-b30 icon-up">
									<div className="icon-bx-lg radius bg-white">
										<Link to="/" className="icon-cell text-primary">
											<i className="flaticon-vector"></i>
										</Link>
									</div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">Development</h4>
										<p>Suspendisse potenti. Pellentesque ornare mattis elit non fermentum. Mauris rhoncus efficitu.</p>
									</div>
								</div>
							</div>
							<div className="col-md-4">
								<div className="icon-bx-wraper style-6 text-center m-b30 icon-up">
									<div className="icon-bx-lg radius bg-white">
										<Link to="/" className="icon-cell text-primary">
											<i className="flaticon-startup"></i>
										</Link>
									</div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">Launching</h4>
										<p>Suspendisse potenti. Pellentesque ornare mattis elit non fermentum. Mauris rhoncus efficitu.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section className="content-inner about-wraper-1" style={{ backgroundImage: "url(images/background/bg15.png)", backgroundSize: "contain", backgroundPosition: "center right", backgroundRepeat: " no-repeat" }}>
					<div className="container">
						<div className="row align-items-center">
							<div className="col-lg-6 m-b30 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s">
								<div className="dz-media left">
									<img src={process.env.PUBLIC_URL + "images/about/pic1.jpg"} alt="" />
								</div>
							</div>
							<div className="col-lg-6 m-b30 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.4s">
								<div className="section-head style-3">
									<h6 className="sub-title text-primary bgl-primary m-b15">ABOUT US</h6>
									<h2 className="title m-b20">We Have Creative Team To Build Your Website Better</h2>
									<p>Proin laoreet leo vel enim gravida, at porttitor metus ultricies. Cras eu velit enim. Vivamus blandit, dolor ut aliquet rutrum, ex elit mattis sapien, non molestie felis neque et nulla. Sed euismod turpis id nibh suscipit semper. Pellentesque dapibus risus arcu.</p>
								</div>
								<ul className="list-check primary m-b30">
									<li>Suspendisse ullamcorper mollis orci in facilisis.</li>
									<li>Etiam orci magna, accumsan varius enim volutpat.</li>
									<li>Donec fringilla velit risus, in imperdiet turpis euismod quis.</li>
									<li>Aliquam pulvinar diam tempor erat pellentesque, accumsan mauri.</li>
								</ul>
								<Link to="/" className="btn btn-primary rounded-xl gradient">Learn More</Link>
							</div>
						</div>
					</div>
				</section>
				<section className="counter-wraper-2 overlay-gradient-dark">
					<div className="counter-inner content-inner-3" style={{ backgroundImage: "url(images/background/bg14.png)", backgroundPosition: "center", backgroundRepeat: "no-repeat" }}>
						<div className="container">
							<div className="row">
								<div className="col-lg-3 col-sm-6 m-b30">
									<div className="dlab-content-bx style-3 text-center">
										<div className="icon-content">
											<h2 className="m-b0 text-primary"><span className="counter m-r5">9875</span>+</h2>
											<span className="title">Satisfied Clients</span>
											<div className="icon-md text-primary">
												<span className="icon-cell">
													<i className="flaticon-smile"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div className="col-lg-3 col-sm-6 m-b30">
									<div className="dlab-content-bx style-3 text-center">
										<div className="icon-content">
											<h2 className="m-b0 text-primary"><span className="counter m-r5">8765</span>+</h2>
											<span className="title">Project Completed</span>
											<div className="icon-md text-primary">
												<span className="icon-cell">
													<i className="flaticon-line-chart"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div className="col-lg-3 col-sm-6 m-b30">
									<div className="dlab-content-bx style-3 text-center">
										<div className="icon-content">
											<h2 className="m-b0 text-primary"><span className="counter m-r5">7894</span>+</h2>
											<span className="title">Project Lunched</span>
											<div className="icon-md text-primary">
												<span className="icon-cell">
													<i className="flaticon-startup"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div className="col-lg-3 col-sm-6 m-b30">
									<div className="dlab-content-bx style-3 text-center">
										<div className="icon-content">
											<h2 className="m-b0 text-primary"><span className="counter m-r5">65</span>+</h2>
											<span className="title">Years Completed</span>
											<div className="icon-md text-primary">
												<span className="icon-cell">
													<i className="flaticon-confetti"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section className="content-inner-2 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
					<div className="container">
						<div className="row">
							<div className="col-lg-12">
								<div className="video-bx style-1 overlay-black-light">
									<img src={process.env.PUBLIC_URL + "images/video/pic1.jpg"} alt="" />
									<div className="video-btn">
										<Link to="/" className="popup-youtube"><i className="flaticon-play"></i></Link>
										<h2 className="title">Watch US</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section className="content-inner-2" style={{ backgroundImage: "url(images/background/bg17.png)", backgroundSize: "contain", backgroundPosition: "center", backgroundRepeat: "no-repeat" }}>
					<div className="container">
						<div className="section-head style-3 text-center">
							<h6 className="sub-title text-primary bgl-primary m-b15">TESTIMONIAL</h6>
							<h2 className="title">See What Our Clients Say’s</h2>
						</div>
						<div className="row">
							<div className="col-md-12">
								<div className="testimonials-wraper-2">
									<div className="swiper-container testimonial-thumbs">
										<div className="swiper-wrapper">
											<div className="swiper-slide">
												<div className="testimonial-pic">
													<img src={process.env.PUBLIC_URL + "images/testimonials/pic1.jpg"} alt="" />
													<div className="shape-bx"></div>
												</div>
											</div>
											<div className="swiper-slide">
												<div className="testimonial-pic">
													<img src={process.env.PUBLIC_URL + "images/testimonials/pic2.jpg"} alt="" />
													<div className="shape-bx"></div>
												</div>
											</div>
											<div className="swiper-slide">
												<div className="testimonial-pic">
													<img src={process.env.PUBLIC_URL + "images/testimonials/pic3.jpg"} alt="" />
													<div className="shape-bx"></div>
												</div>
											</div>
										</div>
									</div>
									<div className="swiper-container testimonial-content">
										<div className="swiper-wrapper">
											<div className="swiper-slide">
												<div className="testimonial-4 quote-right">
													<div className="testimonial-text">
														<strong className="testimonial-name">Cak Dikin</strong>
														<span className="testimonial-position text-primary m-b20">CEO & Founder </span>
														<p>Duis feugiat est tincidunt ligula maximus convallis. Aenean ultricies, mi non vestibulum auctor, erat tortor porttitor ipsum, nec dictum tortor sem eget nunc. Etiam sed facilisis erat. Vestibulum sed posuere augue, ut molestie erat. Nam ipsum tellus, tempus vel ante ut, aliquet finibus dui. Proin lacinia, erat ut feugiat fringilla, tortor eros ultricies sem, sed finibus massa ex sit amet ligula.</p>
													</div>
												</div>
											</div>
											<div className="swiper-slide">
												<div className="testimonial-4 quote-right">
													<div className="testimonial-text">
														<strong className="testimonial-name">Cak Dikin</strong>
														<span className="testimonial-position text-primary m-b20">CEO & Founder </span>
														<p>Duis feugiat est tincidunt ligula maximus convallis. Aenean ultricies, mi non vestibulum auctor, erat tortor porttitor ipsum, nec dictum tortor sem eget nunc. Etiam sed facilisis erat. Vestibulum sed posuere augue, ut molestie erat. Nam ipsum tellus, tempus vel ante ut, aliquet finibus dui. Proin lacinia, erat ut feugiat fringilla, tortor eros ultricies sem, sed finibus massa ex sit amet ligula.</p>
													</div>
												</div>
											</div>
											<div className="swiper-slide">
												<div className="testimonial-4 quote-right">
													<div className="testimonial-text">
														<strong className="testimonial-name">Cak Dikin</strong>
														<span className="testimonial-position text-primary m-b20">CEO & Founder </span>
														<p>Duis feugiat est tincidunt ligula maximus convallis. Aenean ultricies, mi non vestibulum auctor, erat tortor porttitor ipsum, nec dictum tortor sem eget nunc. Etiam sed facilisis erat. Vestibulum sed posuere augue, ut molestie erat. Nam ipsum tellus, tempus vel ante ut, aliquet finibus dui. Proin lacinia, erat ut feugiat fringilla, tortor eros ultricies sem, sed finibus massa ex sit amet ligula.</p>
													</div>
												</div>
											</div>
										</div>
										<div className="swiper-pagination"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section className="content-inner-2" style={{ backgroundImage: "url(images/background/bg16.png)", backgroundSize: "cover", backgroundPosition: "top center", backgroundRepeat: "no-repeat" }}>
					<div className="container">
						<div className="section-head style-3 text-center">
							<h6 className="sub-title text-primary bgl-primary m-b15">OUR BLOG</h6>
							<h2 className="title">Latest News & Blog</h2>
						</div>
						<div className="blog-carousel2 owl-carousel owl-none">
							<div className="item">
								<div className="dlab-blog style-2 m-b10">
									<div className="dlab-media rounded-md">
										<Link to="/"><img src={process.env.PUBLIC_URL + "images/blog/blog-grid-2/pic1.jpg"} alt="" /></Link>
									</div>
									<div className="dlab-info">
										<div className="dlab-meta">
											<ul>
												<li className="post-date"><span>25</span> March</li>
												<li className="post-category">
													<Link to="/">Design</Link>
													<Link to="/">Development</Link>
												</li>
											</ul>
										</div>
										<h4 className="dlab-title"><Link to="/">Maecenas maximus augue eget libero dictum, vitae tempus erat pretium.</Link></h4>
										<p className="m-b20">Nunc convallis sagittis dui eu dictum. Cras sodales id ipsum ac aliquam. Phasellus justo quam, sagittis vel sem congue, vehicula tempus elit. Pellentesque ut scelerisque ante.</p>
										<Link to="/" className="btn-link style-1">Read More</Link>
									</div>
								</div>
							</div>
							<div className="item">
								<div className="dlab-blog style-2 m-b10">
									<div className="dlab-media rounded-md">
										<Link to="/"><img src={process.env.PUBLIC_URL + "images/blog/blog-grid-2/pic2.jpg"} alt="" /></Link>
									</div>
									<div className="dlab-info">
										<div className="dlab-meta">
											<ul>
												<li className="post-date"><span>25</span> March</li>
												<li className="post-category">
													<Link to="/">Design</Link>
													<Link to="/">Development</Link>
												</li>
											</ul>
										</div>
										<h4 className="dlab-title"><Link to="/">Maecenas maximus augue eget libero dictum, vitae tempus erat pretium.</Link></h4>
										<p className="m-b20">Nunc convallis sagittis dui eu dictum. Cras sodales id ipsum ac aliquam. Phasellus justo quam, sagittis vel sem congue, vehicula tempus elit. Pellentesque ut scelerisque ante.</p>
										<Link to="/" className="btn-link style-1">Read More</Link>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div className="content-inner-3 bg-white">
					<div className="container">
						<div className="clients-carousel owl-none owl-carousel m-b30">
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src={process.env.PUBLIC_URL + "images/logo/logo-pink1.png"} alt="" />
									<img className="logo-hover" src={process.env.PUBLIC_URL + "images/logo/logo-light1.png"} alt="" />
								</div>
							</div>
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src={process.env.PUBLIC_URL + "images/logo/logo-pink2.png"} alt="" />
									<img className="logo-hover" src={process.env.PUBLIC_URL + "images/logo/logo-light2.png"} alt="" />
								</div>
							</div>
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src={process.env.PUBLIC_URL + "images/logo/logo-pink3.png"} alt="" />
									<img className="logo-hover" src={process.env.PUBLIC_URL + "images/logo/logo-light3.png"} alt="" />
								</div>
							</div>
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src={process.env.PUBLIC_URL + "images/logo/logo-pink4.png"} alt="" />
									<img className="logo-hover" src={process.env.PUBLIC_URL + "images/logo/logo-light4.png"} alt="" />
								</div>
							</div>
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src={process.env.PUBLIC_URL + "images/logo/logo-pink5.png"} alt="" />
									<img className="logo-hover" src={process.env.PUBLIC_URL + "images/logo/logo-light5.png"} alt="" />
								</div>
							</div>
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src={process.env.PUBLIC_URL + "images/logo/logo-pink6.png"} alt="" />
									<img className="logo-hover" src={process.env.PUBLIC_URL + "images/logo/logo-light6.png"} alt="" />
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</Layout>
	)
}
