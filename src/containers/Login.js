import React, { useState } from 'react'
import Layout from '../components/Layout'
import { Link } from 'react-router-dom'
import { login } from '../helpers/API'

export default function Login({ history }) {
	const [values, setValues] = useState({
		username: "",
		password: "",
	})

	const onsubmit = () => {
		login(values.username, values.password)
			.then(res => {
				localStorage.setItem("token", res.token)
				localStorage.setItem("user", JSON.stringify(res.user))
				history.push("/")
			})
			.catch(err => console.error(err))
	}

	const onValueChange = n => e => setValues({ ...values, [n]: e.target.value })

	return (
		<Layout>
			<div className="page-content bg-white">
				<div className="dlab-bnr-inr style-2 overlay-gradient-dark" style={{ backgroundImage: "url(images/banner/bnr1.jpg)" }}>
					<div className="container">
						<div className="dlab-bnr-inr-entry">
							<h1>Login</h1>
							<nav aria-label="breadcrumb" className="breadcrumb-row">
								<ul className="breadcrumb">
									<li className="breadcrumb-item"><Link to="/">Home</Link></li>
									<li className="breadcrumb-item active" aria-current="page">Login</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
				<div className="content-inner" style={{ backgroundImage: "url(images/background/bg2.png)", backgroundRepeat: "no-repeat" }}>
					<div className="container">
						<div className="row align-items-center">
							<div className="col-lg-6 m-b30 quote-media">
								<div className="dlab-media style-1 move-1">
									<img src="images/team/large/pic1.jpg" alt="" />
								</div>
								<div className="dlab-media style-2 move-2">
									<img src="images/team/large/pic2.jpg" alt="" />
								</div>
								<div className="dlab-media style-3 move-3">
									<img src="images/team/large/pic3.jpg" alt="" />
								</div>
							</div>
							<div className="col-lg-6 m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
								<form className="dlab-form dzForm" method="POST" action="script/contact.php">
									<div className="dzFormMsg"></div>
									<input type="hidden" className="form-control" name="dzToDo" value="Contact" />
									<div className="row">
										<div className="col-sm-6">
											<div className="input-group">
												<input name="dzName" required type="text" className="form-control" placeholder="User Name" onChange={onValueChange("username")} />
											</div>
										</div>
										<div className="col-sm-6">
											<div className="input-group">
												<input name="dzName" required type="password" className="form-control" placeholder="Password" onChange={onValueChange("password")} />
											</div>
										</div>
										<div className="col-sm-12">
											<button name="submit" value="Submit" className="btn btn-primary gradient border-0 rounded-xl"
												onClick={onsubmit}
											>Login
									</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Layout>
	)
}
