import React from 'react'
import Layout from '../components/Layout'
import { Link } from 'react-router-dom'

export default function FAQ() {
	return (
		<Layout>
			<div className="page-content bg-white">
				<div className="dlab-bnr-inr style-2 overlay-gradient-dark" style={{ backgroundImage: "url(images/banner/bnr1.jpg)" }}>
					<div className="container">
						<div className="dlab-bnr-inr-entry">
							<h1>Faq</h1>
							<nav aria-label="breadcrumb" className="breadcrumb-row">
								<ul className="breadcrumb">
									<li className="breadcrumb-item"><Link to="/">Home</Link></li>
									<li className="breadcrumb-item active" aria-current="page">Faq</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
				<section className="content-inner" style={{ backgroundImage: "url(images/background/bg2.png)" }}>
					<div className="container">
						<div className="row">
							<div className="col-lg-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
								<div className="dlab-accordion accordion-sm" id="accordionFaq">
									<div className="card">
										<div className="card-header" id="faqOne">
											<h5 className="dlab-title">
												<Link to="/" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
													Fusce sem ligula, imperdiet sed nisi sit amet ?
										</Link>
											</h5>
										</div>
										<div id="collapseOne" className="collapse show" aria-labelledby="faqOne" data-parent="#accordionFaq">
											<div className="card-body">
												<p className="m-b0">Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.</p>
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header" id="faqTwo">
											<h5 className="dlab-title">
												<Link to="/" className="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
													Maecenas aliquet quam sed tellus cursus ?
										</Link>
											</h5>
										</div>
										<div id="collapseTwo" className="collapse" aria-labelledby="faqTwo" data-parent="#accordionFaq">
											<div className="card-body">
												<p className="m-b0">Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.</p>
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header" id="faqThree">
											<h5 className="dlab-title">
												<Link to="/" className="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseOne">
													Proin blandit sed arcu sed ultricies ?
										</Link>
											</h5>
										</div>
										<div id="collapseThree" className="collapse" aria-labelledby="faqThree" data-parent="#accordionFaq">
											<div className="card-body">
												<p className="m-b0">Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.</p>
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header" id="faqFour">
											<h5 className="dlab-title">
												<Link to="/" className="collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseOne">
													Proin cursus massa ipsum, at lacinia erat amet ?
										</Link>
											</h5>
										</div>
										<div id="collapseFour" className="collapse" aria-labelledby="faqFour" data-parent="#accordionFaq">
											<div className="card-body">
												<p className="m-b0">Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.</p>
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header" id="faqFive">
											<h5 className="dlab-title">
												<Link to="/" className="collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseOne">
													Quisque sem tortor, convallis in arcu finibus massa ?
										</Link>
											</h5>
										</div>
										<div id="collapseFive" className="collapse" aria-labelledby="faqFive" data-parent="#accordionFaq">
											<div className="card-body">
												<p className="m-b0">Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-6 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
								<div className="dlab-accordion accordion-sm" id="accordionFaq1">
									<div className="card">
										<div className="card-header" id="faqOneOne">
											<h5 className="dlab-title">
												<Link to="/" className="collapsed" data-toggle="collapse" data-target="#collapseOneOne" aria-expanded="false" aria-controls="collapseOne">
													Fusce sem ligula, imperdiet sed nisi sit amet ?
										</Link>
											</h5>
										</div>
										<div id="collapseOneOne" className="collapse" aria-labelledby="faqOneOne" data-parent="#accordionFaq1">
											<div className="card-body">
												<p className="m-b0">Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.</p>
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header" id="faqOneTwo">
											<h5 className="dlab-title">
												<Link to="/" data-toggle="collapse" data-target="#collapseOneTwo" aria-expanded="false" aria-controls="collapseOne">
													Maecenas aliquet quam sed tellus cursus ?
										</Link>
											</h5>
										</div>
										<div id="collapseOneTwo" className="collapse show" aria-labelledby="faqOneTwo" data-parent="#accordionFaq1">
											<div className="card-body">
												<p className="m-b0">Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.</p>
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header" id="faqOneThree">
											<h5 className="dlab-title">
												<Link to="/" className="collapsed" data-toggle="collapse" data-target="#collapseOneThree" aria-expanded="false" aria-controls="collapseOne">
													Proin blandit sed arcu sed ultricies ?
										</Link>
											</h5>
										</div>
										<div id="collapseOneThree" className="collapse" aria-labelledby="faqOneThree" data-parent="#accordionFaq1">
											<div className="card-body">
												<p className="m-b0">Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.</p>
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header" id="faqOneFour">
											<h5 className="dlab-title">
												<Link to="/" className="collapsed" data-toggle="collapse" data-target="#collapseOneFour" aria-expanded="false" aria-controls="collapseOne">
													Proin cursus massa ipsum, at lacinia erat amet ?
										</Link>
											</h5>
										</div>
										<div id="collapseOneFour" className="collapse" aria-labelledby="faqOneFour" data-parent="#accordionFaq1">
											<div className="card-body">
												<p className="m-b0">Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.</p>
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header" id="faqOneFive">
											<h5 className="dlab-title">
												<Link to="/" className="collapsed" data-toggle="collapse" data-target="#collapseOneFive" aria-expanded="false" aria-controls="collapseOne">
													Quisque sem tortor, convallis in arcu finibus massa ?
										</Link>
											</h5>
										</div>
										<div id="collapseOneFive" className="collapse" aria-labelledby="faqOneFive" data-parent="#accordionFaq1">
											<div className="card-body">
												<p className="m-b0">Donec suscipit porta lorem eget condimentum. Morbi vitae mauris in leo venenatis varius. Aliquam nunc enim, egestas ac dui in, aliquam vulputate erat. Curabitur porttitor ante ut odio vestibulum, et iaculis arcu scelerisque.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

			</div>
		</Layout>
	)
}
