import React, { useEffect, useState } from 'react'
import Layout from '../components/Layout'
import QuestionCard from '../components/QuestionCard'

import { getCompetencyQuestions } from '../helpers/API'
import { Link } from 'react-router-dom'
import { nanoid } from 'nanoid'
export default function Competency() {
  const [questions, setQuestions] = useState([])

  useEffect(() => {
    getCompetencyQuestions()
      .then(q => setQuestions(q.EICompetencies))
      .catch(err => console.error(err))
  }, [])

  return (
    <Layout>
      <div className="page-content bg-white">
        <div className="dlab-bnr-inr style-2 overlay-gradient-dark" style={{ backgroundImage: "url(../images/banner/bnr1.jpg)" }}>
          <div className="container">
            <div className="dlab-bnr-inr-entry">
              <h1>Competency</h1>
              <nav aria-label="breadcrumb" className="breadcrumb-row">
                <ul className="breadcrumb">
                  <li className="breadcrumb-item"><Link to="/">Tools</Link></li>
                  <li className="breadcrumb-item active" aria-current="page">Competency</li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <div className="content-inner" style={{ backgroundImage: "url(../images/background/bg2.png)", backgroundRepeat: "no-repeat" }}>
          <div className="container">
            <div className="row align-items-center">
              <div className="col-lg-12 m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                <form className="dlab-form dzForm" method="POST" action="script/contact.php">
                  <div className="dzFormMsg"></div>
                      <div className="row">
                      { 
                    questions && (
                    
                        <table className="table table-borderless">
                          <thead>
                            <tr>
                              <th scope="col"></th>
                              <th scope="col">Strongly<br />Disagree</th>
                              <th scope="col">Disagree</th>
                              <th scope="col">Neutral</th>
                              <th scope="col">Agree</th>
                              <th scope="col">Strongly<br />Agree</th>
                            </tr>
                          </thead>
                          {
                            questions.map(question => (
                              <tbody className="col-sm-12 p-2" key={nanoid()}>
                                <QuestionCard type="competency" question={question.question} />
                              </tbody>
                            ))
                          }
                        </table>
                        )
                    }
                    <div className="col-sm-12">
                      <button name="submit" value="Submit" className="btn btn-primary gradient border-0 rounded-xl">Submit Now</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}
