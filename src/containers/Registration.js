import React, { useEffect, useRef, useState } from 'react'
import { nanoid } from 'nanoid'
import Layout from '../components/Layout'
import { Link } from 'react-router-dom'
import { getCountryList } from '../helpers/externalAPI'
import { signup } from '../helpers/API'

export default function Registration() {
	const birthdateRef = useRef("")
	const [values, setValues] = useState({
		firstName: "",
		lastName: "",
		email: "",
		password: "",
		contactISD: "",
		contactNumber: "",
		profilePic: "",
		companyID: 0,
		gender: "",
		birthDate: ""
	})
	const [value,setValue] = useState("");
	const [countries, setCountries] = useState([])

	const onsubmit = () => {
		if(values.password != value)
		{				
			return false;
		}
		else{
			signup(values)
		}
	}

	const onValueChange = n => e => n === "profilePic" ? setValues({ ...values, [n]: e.target.files[0] }) : setValues({ ...values, [n]: e.target.value })

	useEffect(() => {
		getCountryList()
			.then(res => setCountries(res))
			.catch(err => console.error(err))
	}, [])

	return (
		<Layout>
			<div className="page-content bg-white">
				<div className="dlab-bnr-inr style-2 overlay-gradient-dark" style={{ backgroundImage: "url(images/banner/bnr1.jpg)" }}>
					<div className="container">
						<div className="dlab-bnr-inr-entry">
							<h1>Registration</h1>
							<nav aria-label="breadcrumb" className="breadcrumb-row">
								<ul className="breadcrumb">
									<li className="breadcrumb-item"><Link to="/">Home</Link></li>
									<li className="breadcrumb-item active" aria-current="page">Registration</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
				<div className="content-inner" style={{ backgroundImage: "url(images/background/bg2.png)", backgroundRepeat: "no-repeat" }}>
					<div className="container">
						<div className="row align-items-center">
							<div className="col-lg-6 m-b30 quote-media">
								<div className="dlab-media style-1 move-1">
									<img src="images/team/large/pic1.jpg" alt="" />
								</div>
								<div className="dlab-media style-2 move-2">
									<img src="images/team/large/pic2.jpg" alt="" />
								</div>
								<div className="dlab-media style-3 move-3">
									<img src="images/team/large/pic3.jpg" alt="" />
								</div>
							</div>
							<div className="col-lg-6 m-b30 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
								<form className="dlab-form dzForm" method="POST" action="script/contact.php">
									<div className="dzFormMsg"></div>
									<input type="hidden" className="form-control" name="dzToDo" value="Contact" />
									<div className="row">
										<div className="col-sm-6">
											<div className="input-group">
												<input name="dzName" required type="text" className="form-control" placeholder="First Name" onChange={onValueChange("firstName")} />
											</div>
										</div>
										<div className="col-sm-6">
											<div className="input-group">
												<input name="dzOther[last_name" required type="text" className="form-control" placeholder="Last Name" onChange={onValueChange("lastName")} />
											</div>
										</div>
										<div className="col-sm-6">
											<div className="input-group">
												<input name="dzEmail" required type="text" className="form-control" placeholder="Email Address" onChange={onValueChange("email")} />
											</div>
										</div>
										<div className="col-sm-6">
											<div className="input-group">
												<select name="dzOther[gender]" required className="form-control" onChange={onValueChange("gender")}>
													<option value="null">Choose a gender</option>
													<option value="male">Male</option>
													<option value="female">Female</option>
													<option value="other">Other</option>
												</select>
											</div>
										</div>
										<div className="col-sm-6">
											<div className="input-group">
												<input name="dzOther[project_title]" required type="password" className="form-control" placeholder="Password" onChange={onValueChange("password") }/>
											</div>
										</div>										
										<div className="col-sm-6">
											<div className="input-group">
												<input name="dzOther[project_title]" required type="password" className="form-control" placeholder="Password" onChange={ e => setValue(e.target.value) }  />
											</div>
										</div>
										<div className="col-sm-6">
											<div className="input-group">
												<select name="dzOther[country]" id="country" required className="form-control" onChange={onValueChange("contactISD")}>
													<option value="null">Choose country</option>
													{
														(countries !== []) && countries.map(country => country[1] && (
															<option value={country[1]} key={nanoid()}>{country[0]}</option>
														))
													}
												</select>
											</div>
										</div>
										<div className="col-sm-6">
											<div className="input-group">
												<input name="dzOther[phone]" required type="phone" className="form-control" placeholder="Phone no." onChange={onValueChange("contactNumber")} />
											</div>
										</div>
										<div className="col-sm-6">
											<div className="input-group">
												<input name="dzOther[birthdate]" required type="text" className="form-control" placeholder="Select your birthdate" ref={birthdateRef} onFocus={() => birthdateRef.current.type = "date"} onChange={onValueChange("birthDate")} />
											</div>
										</div>
										<div className="col-sm-6">
											<div className="input-group">
												<input name="dzOther[choose_file]" type="file" required className="form-control custom-file-input" onChange={onValueChange("profilePic")} />
												<label className="custom-file-label">Choose a profile pic</label>
											</div>
										</div>
										<div className="col-sm-12">
											<div className="input-group">
												<div className="g-recaptcha" data-sitekey="<!-- Put reCaptcha Site Key -->" data-callback="verifyRecaptchaCallback" data-expired-callback="expiredRecaptchaCallback"></div>
												<input className="form-control d-none" style={{ display: "none" }} data-recaptcha="true" required data-error="Please complete the Captcha" />
											</div>
										</div>
										<div className="col-sm-12">
											<button name="submit" onClick={onsubmit} value="Submit" className="btn btn-primary gradient border-0 rounded-xl">Submit Now
									</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Layout>
	)
}
