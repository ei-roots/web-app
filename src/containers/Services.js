import React from 'react'
import Layout from '../components/Layout'
import { Link } from 'react-router-dom'

export default function Services() {
	return (
		<Layout>
			<div className="page-content bg-white">
				<div className="dlab-bnr-inr style-2 overlay-gradient-dark" style={{ backgroundImage: "url(images/banner/bnr1.jpg)" }}>
					<div className="container">
						<div className="dlab-bnr-inr-entry">
							<h1>Services</h1>
							<nav aria-label="breadcrumb" className="breadcrumb-row">
								<ul className="breadcrumb">
									<li className="breadcrumb-item"><Link to="/">Home</Link></li>
									<li className="breadcrumb-item active" aria-current="page">Services</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
				<section className="content-inner-2" style={{ backgroundImage: "url(images/background/bg2.png)" }}>
					<div className="container">
						<div className="row">
							<div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.1s">
								<div className="icon-bx-wraper style-3 box-hover m-b30">
									<div className="icon-bx-sm radius bgl-primary">
										<Link to="/" className="icon-cell">
											<i className="flaticon-office"></i>
										</Link>
									</div>
									<div className="wraper-effect"></div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">Strategy & Research</h4>
										<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
								<div className="icon-bx-wraper style-3 box-hover active m-b30">
									<div className="icon-bx-sm radius bgl-primary">
										<Link to="/" className="icon-cell">
											<i className="flaticon-website"></i>
										</Link>
									</div>
									<div className="wraper-effect"></div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">24X7 Support</h4>
										<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.3s">
								<div className="icon-bx-wraper style-3 box-hover m-b30">
									<div className="icon-bx-sm radius bgl-primary">
										<Link to="/" className="icon-cell">
											<i className="flaticon-pie-charts"></i>
										</Link>
									</div>
									<div className="wraper-effect"></div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">Web Development</h4>
										<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.4s">
								<div className="icon-bx-wraper style-3 box-hover m-b30">
									<div className="icon-bx-sm radius bgl-primary">
										<Link to="/" className="icon-cell">
											<i className="flaticon-dollar"></i>
										</Link>
									</div>
									<div className="wraper-effect"></div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">Web Solution</h4>
										<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.5s">
								<div className="icon-bx-wraper style-3 box-hover m-b30">
									<div className="icon-bx-sm radius bgl-primary">
										<Link to="/" className="icon-cell">
											<i className="flaticon-line-graph"></i>
										</Link>
									</div>
									<div className="wraper-effect"></div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">SEO & Marketing</h4>
										<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
								<div className="icon-bx-wraper style-3 box-hover m-b30">
									<div className="icon-bx-sm radius bgl-primary">
										<Link to="/" className="icon-cell">
											<i className="flaticon-help"></i>
										</Link>
									</div>
									<div className="wraper-effect"></div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">Growth Tracking</h4>
										<p>Vestibulum a efficitur ex. Ut iaculis dapibus iaculis. Praesent lacus magna, rhoncus quis magna quis, pellentesque luctus leoam iaculis venenatis.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section className="content-inner-2 about-wraper-1" style={{ backgroundImage: "url(images/background/bg15.png)", backgroundSize: "contain", backgroundPosition: "center right", backgroundRepeat: "no-repeat" }}>
					<div className="container">
						<div className="row align-items-center">
							<div className="col-lg-6 m-b30 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s">
								<div className="dz-media left">
									<img src="images/about/pic1.jpg" alt="" />
								</div>
							</div>
							<div className="col-lg-6 m-b30 wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.4s">
								<div className="section-head style-3">
									<h6 className="sub-title text-primary bgl-primary m-b15">Our Skills</h6>
									<h2 className="title m-b20">We Have Best Experience In  All Our Business Services</h2>
								</div>

								<div className="progress-bx overflow-hidden mb-3">
									<div className="progress-info">
										<span className="title">SEO</span>
										<span className="progress-value">80%</span>
									</div>
									<div className="progress mb-3">
										<div className="progress-bar wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.6s" style={{ width: "80%" }}></div>
									</div>
								</div>
								<div className="progress-bx overflow-hidden mb-3">
									<div className="progress-info">
										<span className="title">Designing</span>
										<span className="progress-value">90%</span>
									</div>
									<div className="progress mb-3">
										<div className="progress-bar wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.6s" style={{ width: "90%" }}></div>
									</div>
								</div>
								<div className="progress-bx overflow-hidden mb-3">
									<div className="progress-info">
										<span className="title">Development</span>
										<span className="progress-value">85%</span>
									</div>
									<div className="progress mb-3">
										<div className="progress-bar wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.6s" style={{ width: "85%" }}></div>
									</div>
								</div>
								<div className="progress-bx overflow-hidden">
									<div className="progress-info">
										<span className="title">Marketing</span>
										<span className="progress-value">70%</span>
									</div>
									<div className="progress mb-3">
										<div className="progress-bar wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.6s" style={{ width: "70%" }}></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section className="content-inner-2">
					<div className="container">
						<div className="row align-items-center">
							<div className="col-lg-5 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.2s">
								<div className="section-head style-3 mb-4">
									<h6 className="sub-title text-primary bgl-primary m-b15">OUR FEATURES</h6>
									<h2 className="title">Behind The Story Of Slack Digital Agency</h2>
								</div>
								<p>Suspendisse potenti. Donec vel massa ut justo efficitur lacinia non ut felis. Etiam euismod, magna et efficitur ullamcorper, justo justo suscipit tellus, quis egestas lectus nulla ac velit. Morbi consequat vehicula tincidunt.</p>
								<p>Suspendisse purus nibh, lacinia ut rutrum eu, tincidunt eleifend ligula. Maecenas tristique est vitae erat semper pellentesque.</p>
								<p className="m-b30">auris sollicitudin tincidunt libero ac suscipit. Quisque ullamcorper consequat dui ut molestie.</p>
							</div>
							<div className="col-lg-7">
								<div className="icon-bx-wraper style-3 left box-hover m-r100 m-b30 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.2s">
									<div className="icon-bx-sm radius">
										<Link to="/" className="icon-cell">
											<i className="flaticon-idea"></i>
										</Link>
									</div>
									<div className="wraper-effect"></div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">Idea & Analysis Gathering</h4>
										<p>Maecenas laoreet efficitur sagittis. Aliquam eleifend nisl leo, sit amet consequat augue mattis varius. Suspendisse rhoncus nisl.</p>
									</div>
								</div>
								<div className="icon-bx-wraper style-3 left box-hover active m-l100 m-b30 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.4s">
									<div className="icon-bx-sm radius">
										<Link to="/" className="icon-cell">
											<i className="flaticon-coding"></i>
										</Link>
									</div>
									<div className="wraper-effect"></div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">Design & Developing</h4>
										<p>Maecenas laoreet efficitur sagittis. Aliquam eleifend nisl leo, sit amet consequat augue mattis varius. Suspendisse rhoncus nisl.</p>
									</div>
								</div>
								<div className="icon-bx-wraper style-3 left box-hover m-r100 m-b30 wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.6s">
									<div className="icon-bx-sm radius">
										<Link to="/" className="icon-cell">
											<i className="flaticon-rocket"></i>
										</Link>
									</div>
									<div className="wraper-effect"></div>
									<div className="icon-content">
										<h4 className="dlab-title m-b15">Testing & Lunching</h4>
										<p>Maecenas laoreet efficitur sagittis. Aliquam eleifend nisl leo, sit amet consequat augue mattis varius. Suspendisse rhoncus nisl.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section className="content-inner-2" style={{ backgroundImage: "url(images/background/bg17.png)", backgroundSize: "contain", backgroundPosition: "center", backgroundRepeat: "no-repeat" }}>
					<div className="container">
						<div className="section-head style-3 text-center">
							<h6 className="sub-title text-primary bgl-primary m-b15">TESTIMONIAL</h6>
							<h2 className="title">See What Our Clients Say’s</h2>
						</div>
						<div className="row">
							<div className="col-md-12">
								<div className="testimonials-wraper-2">
									<div className="swiper-container testimonial-thumbs">
										<div className="swiper-wrapper">
											<div className="swiper-slide">
												<div className="testimonial-pic">
													<img src="images/testimonials/pic1.jpg" alt="" />
													<div className="shape-bx"></div>
												</div>
											</div>
											<div className="swiper-slide">
												<div className="testimonial-pic">
													<img src="images/testimonials/pic2.jpg" alt="" />
													<div className="shape-bx"></div>
												</div>
											</div>
											<div className="swiper-slide">
												<div className="testimonial-pic">
													<img src="images/testimonials/pic3.jpg" alt="" />
													<div className="shape-bx"></div>
												</div>
											</div>
										</div>
									</div>
									<div className="swiper-container testimonial-content">
										<div className="swiper-wrapper">
											<div className="swiper-slide">
												<div className="testimonial-4 quote-right">
													<div className="testimonial-text">
														<strong className="testimonial-name">Cak Dikin</strong>
														<span className="testimonial-position text-primary m-b20">CEO & Founder </span>
														<p>Duis feugiat est tincidunt ligula maximus convallis. Aenean ultricies, mi non vestibulum auctor, erat tortor porttitor ipsum, nec dictum tortor sem eget nunc. Etiam sed facilisis erat. Vestibulum sed posuere augue, ut molestie erat. Nam ipsum tellus, tempus vel ante ut, aliquet finibus dui. Proin lacinia, erat ut feugiat fringilla, tortor eros ultricies sem, sed finibus massa ex sit amet ligula.</p>
													</div>
												</div>
											</div>
											<div className="swiper-slide">
												<div className="testimonial-4 quote-right">
													<div className="testimonial-text">
														<strong className="testimonial-name">Cak Dikin</strong>
														<span className="testimonial-position text-primary m-b20">CEO & Founder </span>
														<p>Duis feugiat est tincidunt ligula maximus convallis. Aenean ultricies, mi non vestibulum auctor, erat tortor porttitor ipsum, nec dictum tortor sem eget nunc. Etiam sed facilisis erat. Vestibulum sed posuere augue, ut molestie erat. Nam ipsum tellus, tempus vel ante ut, aliquet finibus dui. Proin lacinia, erat ut feugiat fringilla, tortor eros ultricies sem, sed finibus massa ex sit amet ligula.</p>
													</div>
												</div>
											</div>
											<div className="swiper-slide">
												<div className="testimonial-4 quote-right">
													<div className="testimonial-text">
														<strong className="testimonial-name">Cak Dikin</strong>
														<span className="testimonial-position text-primary m-b20">CEO & Founder </span>
														<p>Duis feugiat est tincidunt ligula maximus convallis. Aenean ultricies, mi non vestibulum auctor, erat tortor porttitor ipsum, nec dictum tortor sem eget nunc. Etiam sed facilisis erat. Vestibulum sed posuere augue, ut molestie erat. Nam ipsum tellus, tempus vel ante ut, aliquet finibus dui. Proin lacinia, erat ut feugiat fringilla, tortor eros ultricies sem, sed finibus massa ex sit amet ligula.</p>
													</div>
												</div>
											</div>
										</div>
										<div className="swiper-pagination"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div className="content-inner-3 bg-white">
					<div className="container">
						<div className="clients-carousel owl-none m-b30 owl-carousel">
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src="images/logo/logo-pink1.png" alt="" />
									<img className="logo-hover" src="images/logo/logo-light1.png" alt="" />
								</div>
							</div>
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src="images/logo/logo-pink2.png" alt="" />
									<img className="logo-hover" src="images/logo/logo-light2.png" alt="" />
								</div>
							</div>
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src="images/logo/logo-pink3.png" alt="" />
									<img className="logo-hover" src="images/logo/logo-light3.png" alt="" />
								</div>
							</div>
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src="images/logo/logo-pink4.png" alt="" />
									<img className="logo-hover" src="images/logo/logo-light4.png" alt="" />
								</div>
							</div>
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src="images/logo/logo-pink5.png" alt="" />
									<img className="logo-hover" src="images/logo/logo-light5.png" alt="" />
								</div>
							</div>
							<div className="item">
								<div className="clients-logo">
									<img className="logo-main" src="images/logo/logo-pink6.png" alt="" />
									<img className="logo-hover" src="images/logo/logo-light6.png" alt="" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Layout>
	)
}
